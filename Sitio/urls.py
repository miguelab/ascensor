from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf.urls import url
from . import views

urlpatterns = [
    # Generales
    path('', views.index, name="index"),
    path('Panel', views.panel, name="panel"),
    path('Login', views.login, name="login"),
    path('Login/Iniciar',views.loginIniciar,name="loginIniciar"),
    path('Logout',views.cerrarSession,name="cerrarSession"),

    # Asignación
    path('Asignacion/', views.agregarAsignacion, name="agregarAsignacion"),
    path('Asignacion/Crear', views.crearAsignacion, name="crearAsignacion"),
    path('Asignacion/Listar', views.listarAsignacion, name="listarAsignacion"),
    path('Asignacion/Listar/Tomar/<int:idAsignacion>/<int:idCliente>/<int:idTecnico>', views.tomarAsignacion, name="tomarAsignacion"),

    # Ascensor
    path('Registro/Ascensor', views.agregarAscensor, name="agregarAscensor"),
    path('Registro/Crear/Ascensor', views.crearAscensor, name="crearAscensor"),
    path('Listar/Ascensor', views.listarAscensor, name="listarAscensor"),
    path('Listar/Editar/Ascensor/<int:id>', views.editarAscensor, name="editarAscensor"),
    path('Listar/Borrar/Ascensor/<int:id>', views.borrarAscensor, name="borrarAscensor"),

    # Cliente
    path('Registro/Cliente', views.registroCliente, name="registroCliente"),
    path('Registro/Crear/Cliente', views.crearCliente, name="crearCliente"),
    path('Listar/Cliente', views.listarCliente, name="listarCliente"),
    path('Listar/Editar/Cliente/<int:id>', views.editarCliente, name="editarCliente"),
    path('Listar/Borrar/Cliente/<int:id>', views.borrarCliente, name="borrarCliente"),

    # Orden
    path('Registro/Orden/', views.generarOrden, name="generarOrden"),
    path('Registro/Crear/Orden', views.crearOrden, name="crearOrden"),
    path('Listar/Orden', views.listarOrden, name="listarOrden"),
    path('Listar/Editar/Orden/<int:id>', views.editarOrden, name="editarOrden"),
    path('Listar/Borrar/Orden/<int:id>', views.borrarOrden, name="borrarOrden"),
    
    # Técnico
    path('Registro/Tecnico', views.registroTecnico, name="registroTecnico"),
    path('Registro/Crear/Tecnico', views.crearTecnico, name="crearTecnico"),
    path('Listar/Tecnico', views.listarTecnico, name="listarTecnico"),
    path('Listar/Editar/Tecnico/<int:id>', views.editarTecnico, name="editarTecnico"),
    path('Listar/Borrar/Tecnico/<int:id>', views.borrarTecnico, name="borrarTecnico"),
]