from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)

# Asignar
class Asignacion(models.Model):
    idTecnico = models.IntegerField(blank=True,null=True)
    idCliente = models.IntegerField(blank=True,null=True)
    estado = models.IntegerField(blank=True,null=True)

# Ascensor
class Ascensor(models.Model):
    identificador = models.CharField(max_length=40)
    modelo = models.CharField(max_length=120)

# Ciudad
class Ciudad(models.Model):
    descripcion = models.CharField(max_length=40)

# Cliente
class Cliente(models.Model):
    nombre = models.CharField(max_length=40)
    direccion = models.CharField(max_length=120)
    idCiudad = models.IntegerField(blank=True,null=True)
    idComuna = models.IntegerField(blank=True,null=True)
    telefono = models.IntegerField(blank=True,null=True)
    email = models.CharField(max_length=40)

# Comuna
class Comuna(models.Model):
    descripcion = models.CharField(max_length=40)
    idCiudad = models.IntegerField(blank=True,null=True)

# Crear usuario técnico
class TecnicoBaseUserManager(BaseUserManager):
    def create_user(self, email, password):
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

# Técnico
class Tecnico(AbstractBaseUser, PermissionsMixin):
    nombre = models.CharField(max_length=40)
    direccion = models.CharField(max_length=120)
    idCiudad = models.IntegerField(blank=True,null=True)
    idComuna = models.IntegerField(blank=True,null=True)
    telefono = models.IntegerField(blank=True,null=True)
    email = models.CharField(max_length=40, unique=True)
    password = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    objects = TecnicoBaseUserManager()

    def get_full_name(self):
        return self.nombre

# Ordenes de trabajo
class Orden(models.Model):
    idCliente = models.IntegerField(blank=True,null=True)
    fechaOrden = models.DateField(blank=True,null=True)
    horaInicio = models.TimeField(blank=True,null=True)
    horaTermino = models.TimeField(blank=True,null=True)
    idAscensor = models.IntegerField(blank=True,null=True)
    fallasEncontradas = models.CharField(max_length=512)
    piezasRemplazadas = models.CharField(max_length=512)
    idTecnico = models.IntegerField(blank=True,null=True)